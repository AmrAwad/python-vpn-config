#!/bin/bash

# This script downloads chosen ovpn files from:
# https://vpn.hidemyass.com/vpn-config/UDP/ and creates a password file
# and adds it to the configurations.

# make directory for ovpn
mkdir ovpn && cd ovpn

# List of VPNs to download
wget https://vpn.hidemyass.com/vpn-config/UDP/Afghanistan.Kabul.UDP.ovpn 
wget https://vpn.hidemyass.com/vpn-config/UDP/AlandIslands.Mariehamn.UDP.ovpn
wget https://vpn.hidemyass.com/vpn-config/UDP/Albania.Tirana.UDP.ovpn 

# Create a password file
echo "USERNAME" > ovpn.pass
echo "PASS" >> ovpn.pass

# Add password file to all the downloaded config files
sed -i "s|auth-user-pass|auth-user-pass ovpn/ovpn.pass|" *.ovpn
