# Python VPN Connector

This repository provides a command line tool to connect automatically to a random pre-configured ovpn file from a folder, and confirm that the public IP has changed, if it doesn't successfully connect after a certain number of trials, it will choose a different OVPN file and retry.

## Setup

1. On a clean ubuntu install, you'll need to install the `openvpn` command line tool using:
```
sudo apt-get update
sudo apt-get install openvpn -y
```
2. Add the list of the ovpn files you'd like to download to `download-ovpn.sh` script, like this:
```bash
# List of VPNs to download
wget https://vpn.hidemyass.com/vpn-config/UDP/Afghanistan.Kabul.UDP.ovpn 
wget https://vpn.hidemyass.com/vpn-config/UDP/AlandIslands.Mariehamn.UDP.ovpn
wget https://vpn.hidemyass.com/vpn-config/UDP/Albania.Tirana.UDP.ovpn 
```
You can find the complete list of OVPN files available for HMA [here](https://vpn.hidemyass.com/vpn-config/).

3. Update the HMA username and password in `download-ovpn.sh`:
```bash
# Create a password file
echo "USERNAME" > vpnbook.pass
echo "PASS" >> vpnbook.pass
```

4. Run the script using the following commands:
```bash
chmod +x download-ovpn.sh
./download-ovpn.sh
```

This will create a folder called `ovpn`, inside you'll find the downloaded & configured ovpn files along with `ovpn.pass` file which contains the username and password.

## Usage

The command line takes a single argument 'connect' or 'disconnect':
```
$ python vpnConfig.py -h
usage: vpnConfig.py [-h] {connect,disconnect}

VPN Connector

positional arguments:
  {connect,disconnect}

optional arguments:
  -h, --help            show this help message and exit
```

To connect to a random VPN use the following command:
```
$ python vpnConfig.py connect
```

To disconnect, use the following command:
```
$ python vpnConfig.py disconnect
```
