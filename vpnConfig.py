import os
import glob
import random
import time
import json
import requests
import argparse


class VpnConfig():
    """docstring for VpnConfig"""

    def __init__(self):
        self.close_openvpn()

        # Get list of available VPNs
        self.vpnList = glob.glob('ovpn/*.ovpn')
        self.connected = False

    def connect_to_vpn(self):
        current_ip = self.get_current_ip()
        while self.connected is False:
            # Get a random VPN from the list
            randomVPN = random.choice(self.vpnList)
            print("[log] Chosen VPN: " + randomVPN)

            # Connect to VPN (exit code 0 means successful command)
            self.close_openvpn()
            os.system("sudo openvpn --daemon --config " + randomVPN)
            os.system("echo [log] Exit Code: $?")

            self.confirm_ip_change(current_ip)
            if self.connected is False:
                print("[log] VPN Failed! Trying a different VPN ...")

    def confirm_ip_change(self, current_ip, maxRetries=5, wait=1):
        for retry in range(maxRetries):
            time.sleep(wait)
            new_ip = self.get_current_ip()
            if new_ip != current_ip:
                print("[log] VPN Connection successful ... ")
                self.connected = True
                break
            else:
                print("[log] Check #{0} Failed! rechecking in {1} seconds ... ".format(retry, wait))

    def get_current_ip(self):
        response = requests.get("https://api.ipify.org?format=json")
        current_ip = json.loads(response.text)['ip']
        print("[log] Current IP: " + current_ip)
        return current_ip

    def close_openvpn(self):
        print("[log] Closing Openvpn")
        os.system("killall openvpn &> /dev/null")

    def __del__(self):
        self.close_openvpn()


def main():

    parser = argparse.ArgumentParser(description='VPN Connector')
    parser.add_argument('type', choices=['connect', 'disconnect'],
                        default='connect', type=str)

    args = parser.parse_args()

    vpn = VpnConfig()
    if args.type == 'connect':
        vpn.connect_to_vpn()
    elif args.type == 'disconnect':
        vpn.close_openvpn()

if __name__ == '__main__':
    main()
